const OPS = {
    UP: 'up',
    DOWN: 'down',
    LEFT: 'left',
    RIGHT: 'right',
    STOP: 'stop'
};

const GRAPH_OPTIONS = {
    maintainAspectRatio: false,
    scales: {
        y: {
            beginAtZero: true
        }
    },
    plugins: {
        colors: {
            enabled: true
        }
    }
}

class Data {
    constructor(label, value) {
        this.label = label;
        this.value = value;
    }
}

const onControlClick = (op) => {
    console.log('op: ', op);
};

let time = 0;

const pollData = (src) => {
    return [...Array(6).keys()]
        .map(i => time + i)
        .map(i => new Data(i, Math.floor(Math.random() * 20)));
};

const pollDataAfter = (src, callback) => {
    window.setTimeout(() => {
        callback(pollData(src));
    }, 3000);
}

window.onload = (e) => {
    const colors = ['blue', 'green', 'red', 'orange'];
    [...Array(4).keys()]
       .map(i => i + 1)
       .forEach(i => {
           window.setTimeout(() => {
               const ds = 'Data Source ' + i;
               const color = colors[i - 1];
               const mapData = (data) => {
                   return {
                       labels: data.map(i => i.label),
                       datasets: [{
                           label: ds,
                           data: data.map(i => i.value),
                           borderWidth: 1,
                           borderColor: color,
                           backgroundColor: color
                       }]
                   };
               };
               // https://www.chartjs.org/docs/latest/
               let chart = new Chart(document.getElementById('graph-' + i), {
                   type: 'line',
                   data: mapData(pollData(ds)),
                   options: GRAPH_OPTIONS
               });
               const pollCallback = data => {
                   console.log('Acquired new mock data');
                   chart.data = mapData(data);
                   chart.update('none');
                   if (i === 4) {
                       time++;
                   }
                   pollDataAfter(ds, pollCallback);
               };
               pollDataAfter(ds, pollCallback);
           }, Math.floor(Math.random() * 3) * 1000);
        });
    window.setTimeout(() => {
        const video = document.createElement('div');
        video.id = 'video';

        const boundingBox = document.createElement('div');
        boundingBox.id = 'video-bounding-box';

        video.appendChild(boundingBox);

        document.getElementById('video-loader').remove();
        document.getElementById('video-container').prepend(video);
    }, 2000);
}

console.log('Script loaded');
